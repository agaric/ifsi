# Search

Faceted filtering and search on the site are powered by Apache Solr.

It is controlled by a module that lives with the Find It Profile, see:
`web/profiles/contrib/findit/modules/custom/drutopia_findit_search`

* Taxonomy terms are indexed by *name*, not ID.  If a term name changes (Neighborhood, Service, Activity, Age, Grade, etc.) **the Solr index must be deleted and rebuilt.**.


## Debugging

### Getting it running locally

Do this first:

`{'delete': {'query': '*:*'}}` per
https://stackoverflow.com/questions/23228727/deleting-solr-documents-from-solr-admin/48007194#48007194

Delete index and re-run indexing after a change to content:


```
# Clear index fully.
drush sapi-sc solr_development_
drush sapi-c
# Now, re-index.
drush sapi-i
```

Probably not both of those are necessary but clearing the Solr index is needed after getting fresh database content.

### Querying Solr directly

Note: To use the UI, instead of curl, go to http://findit-dev.test:8983/solr/#/drupal/query

Trying queries against Solr directly (note the query is different of course than the URL query used on the Drupal pages).

```
curl -i 'https://findit-dev.test:8982/solr/drupal/select?q=*:*&fq=bs_status:0&fq=sm_neighborhoods:Riverside&fq=sm_services=Afterschool'
```

It does need quotation marks on anything with a space, even if you %19 the space.

Example fq parameter pairs:

* ss_types=findit_program
* 

Use an IDE with Vagrant-provided Xdebug (PHPStorm works without changes: ?XDEBUG_START=PHPSTORM).


Adding tags is destroying the whole query, so trying urlencode the curly braces around the tag— now at least getting results, but different results from Drupal.

```
curl -i 'https://findit-dev.test:8982/solr/drupal/select?q=*:*&fq=bs_status:0&fq=sm_neighborhoods:Riverside&fq=sm_services=Afterschool&facet=true&facet.mincount=1&'
```
## Solr Instances

The Solr server is a single server inside Azure services with both Solr 6 and Solr 8 on it. If you ssh to the findit-vm, you can then ssh to 10.0.0.6. There are folders in the home directory to get to either Solr core configuration structure.

Solr 8 is available on port 8983, and Solr 6 is on 8984

For example, the following settings in Drupal will reach the test.finditcambridge.org instance (Solr8):
Solr srv: 10.0.0.6
http port: 8983
core: drupal_test

The Agaric secrets/ssh config contains port forward definitions for reaching the Solr Admin UI:
http://127.0.0.1:8983 brings up Solr8
http://127.0.0.1:8984 brings up Solr6

## Updating or creating a Solr 8 core

The configuration used by Solr is sourced from the Drupal installation. In the settings for the Search API (admin/config/search/search-api), select the drop-down for Operations for the Solr Server, and select `get config.zip` option.

Unzip file and tgz it instead - the Solr server does not have zip.
Copy config to findit-vm (e.g. `scp solr_8.x_config.tgz findit-vm:`)
SSH to findit-vm, and push the config over to 10.0.0.6 (e.g. `scp solr_8.x_config.tgz 10.0.0.6:`)
SSH (from findit-vm) to 10.0.0.6. If this fails, you may not have ssh-agent enabled with forwarding (when you connect to findit-vm, try adding -A)
Change the owner of the tgz copied there: `sudo chown solr solr_8.x_config.tgz` file to solr, and move it to the config folder: `mv solr_8.x_config.tgz /data/solr8-data/data`
Change to the solr user in the data folder with `su - i -u solr && cd /data/solr8/data`
If you are creating a new core, first create the root folder and subfolders for it (e.g. `mkdir drupal_live && mkdir drupal_live/data && mkdir drupal_live/conf`)
Untar (or unzip) the file in the core's folder: `mv solr_8.x_config.tgz drupal_test && tar xzf solr_8.x_config.tgz`
Arrange the configuration files with a couple moves: `mv *.txt conf && mv *.xml conf`
The files remaining should stay in the current folder (e.g. drupal_stage/core.properties), but the source zip can be removed: `rm solr_8.x_config.tgz`
 bring up the Solr UI and click into Core Admin
If creating a new core: click "Add Core". In the dialog, specify a name, and set the instance dir to just the name of the created folder (e.g. `drupal_live`). The remaining values can be left at their default.
If not creating a core, simply click the reload button.

## Reference

https://lucene.apache.org/solr/guide/8_1/faceting.html

https://www.drupal.org/docs/8/modules/search-api-solr
