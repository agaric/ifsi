# IFSI One Stop Center README

Based on the Find It Program Locator and Event Discovery platform.

Find It makes organizations and programs easily findable through a curated directory. Originated in Cambridge, Massachusetts with Leo Burd, Agaric develops it and is now will be modified into an Immigrant Navigator for newly arrived peoples.

This is a composer based installer and virtual machine setup for developers working on the [Find It Program Locator distribution](https://gitlab.com/find-it-program-locator/).

## Installation

### Prerequisites

1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) (check your app store or package manager)
2. Install [Vagrant](https://www.vagrantup.com/downloads.html) (check your app store or package manager)

If at any point you want to delete your database so you can start development fresh with a new install you can `rm web/sites/default/files/.ht.sqlite`.

# Development

Currently we presume people that are contributing significantly to development will spin up this project in a virtual machine:

```
git clone git@gitlab.com:find-it-program-locator/findit-dev.git
cd findit-dev
vagrant up
```

*Note: DDEV also works.*

## Destructively update your development environment

```
composer update
composer fresh-start
```

Breaking that down what those two commands do...

**Potentially wipe out unpushed local code and exported features** and **update to the latest code changes others have made** to [Find It profile](https://gitlab.com/find-it-program-locator/findit), [Find It Organization](https://gitlab.com/find-it-program-locator/drutopia_findit_organization), [Find It Program](https://gitlab.com/find-it-program-locator/drutopia_findit_program), etc with a Composer update:

```
composer update
```

*Note: A patch to Facets fails to apply (because we're using the dev version that incorporates the patch) but we cannot stop it from trying to apply (because the patch is called for by a contrib module that does not presume the dev version).  That's fine, but sometimes, for Ben on DDEV at least, due to what [appears to be a bug in composer patches](https://github.com/cweagans/composer-patches/issues/277), Facets module sometimes doesn't get installed for.  Check `ls web/modules/contrib/facets` and, if it's not there, run `composer update` a second time.*

**Wipe out your local database and un-Featurized configuration** and re-install the distribution with:

```
composer fresh-start
```

It is an alias for:

```
drush -y si findit && drush -y en drutopia_dev_findit && drush uli
drush locale:import en modules/contrib/findit_interface_text/overrides/custom-translations.en.po --type=customized --override=all
```

## Bringing down content from CI replacing anything you have locally

This too will **wipe out your local database and un-Featurized configuration** and replace it with the content on CI:

```
drush -y sql-sync @drutopia.ci @self
drush -y rsync @drutopia.ci:%files @self:%files
```

## Custom interface translations

Export manually at `/admin/config/regional/translate/export` until [pull request 3253 add command to export .po files](https://github.com/drush-ops/drush/pull/3253) is merged into Drush.

Then add (and commit) the strings you customized to the `findit_interface_text` module's [`overrides/custom-translations.en.po` file](https://gitlab.com/find-it-program-locator/findit_interface_text/blob/8.x-1.x/overrides/custom-translations.en.po).

Import with:

```
drush locale:import en web/modules/contrib/findit_interface_text/overrides/custom-translations.en.po --type=customized --override=all
```

## Configuration synchronization is used for individual sites, not for the installation profile

Note that while there is a `config/sync` directory, it is *not* used to install sites as part of development nor will it be used for most active sites (which we expect will be able to account for differences from the distribution with config split or config override).  Instead, the local configuration directory is for the convenience of developers, who can commit where they are and see what has changed, or to send to another developer, towards the end of [packaging all configuration up into features](http://docs.drutopia.org/en/latest/new-content-feature.html ).


## Deployment

Agaric is currently using a Platform as a Service version of Drutopia with additional modules.

Set up [drutopia_host](https://gitlab.com/drutopia-platform/drutopia_host) and [hosting_private](https://gitlab.com/drutopia-platform/hosting_private), as documented in hosting private.

Then use [ahoy](https://github.com/ahoy-cli/ahoy/), from within the hosting_private directory.

Ensure all three related repositories are up-to-date with:

```
ahoy git-pull-all
```

If Agaric's Drutopia PaaS base has changed (the composer.lock in this directory), produce and push a new build:

```
ahoy deploy-build agaric
```

To deploy this build to an instance of the site plus config, templates, styles:

```
ahoy deploy-site agaric_test
```

(And likewise for `agaric_live`.)

Then record the deployment. This applies to both deploy-site and deploy-build record keeping:
Navigate to `/drutopia_host/build_artifacts`
Add all new files generated with `git add . `
Commit the files with a message on what you deployed with `git commit -m "Add artifacts"`
Push the changes to the repo with `git push`

If you need to overwrite live configuration (only after confirming you've committed any parts you want to keep) you can use ahoy for that too with `deploy-site-force`.

### Drupal settings file management

The Drupal settings files are also managed by Drutopia. On each deploy, the file is generated and replaces the `settings.local.php` that is on the server with whatever settings will match the Drutopia configuration. Therefore, in order to add settings, you must edit the vault settings using:

```
ahoy vault-edit
```

Look for the Yaml section for agaric_live, or agaric_test, as appropriate, and edit the php_settings_code section to add items to that site's setting overrides. For example:

```
      ...
      online: True
      php_settings_code: |2
        $settings['super_secret'] = p@ssw0rd;
        $config['life.answer'] = 42;
      server_aliases:
        - agaric.com
      ...
```

## See also

See [DEVELOPMENT.md](DEVELOPMENT.md) for more, particularly regarding feature development.

See [SEARCH.md](SEARCH.md) for notes on developing the Solr-powered facet filtering and full-text search.

See [CUSTOMIZATIONS.md](CUSTOMIZATIONS.md) for notes on recommended approaches to necessary and optional modifications for specific instances of the distribution.

See [PACKAGING.md](PACKAGING.md) for our approach to packaging up the distribution for easy use by sites on the 'platform' (same codebase).

See [DEBUG.md](DEBUG.md) for troubleshooting and debugging recommendations, like how to read logs, especially for live, test, and continuous integration sites.
