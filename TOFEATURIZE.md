# Cambridge custom work to Featurize

Some work done directly on Find It Cambridge should be featurized into the distribution.

## Opportunity categories view

Add:

* views.view.findit_opportunity_categories
* core.entity_view_display.taxonomy_term.findit_services.teaser.yml
* core.entity_view_mode.taxonomy_term.teaser.yml

### Taxonomy term teaser

Note: This was created new.

* taxonomy_term.teaser


## Opportunities listing

Changes to not have main title be Trash:

* sync/views.view.opportunities.yml

Changes to event display:

* core.entity_view_display.node.findit_event.default.yml see for instance https://gitlab.com/find-it-program-locator/findit/issues/249#note_244889958


## Secondary footer menu

* system.menu.secondary-footer.yml


... so i stopped keeping track really a while ago, but here's a big one:


Reference blocks from within paragraphs, specifically for the Today's Events block:
https://gitlab.com/agaric/sites/find-it-cambridge-config/commit/70181e32fcd6eb625200572fccd26061e61e3bd0
