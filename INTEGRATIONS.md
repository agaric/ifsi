# Integrations

## Cambridge Public Library

Term mapping - https://docs.google.com/document/d/1pyJaBaSdjh8y9okDO1Uq7tOZjR2hOtr6vfkX2sLPhpQ/edit

When a contact is not provided, an event is assigned the "Library" contact - https://www.finditcambridge.org/node/9

Library events are imported every midnight.  In `crontab -e`:

```
# Thanks to https://www.the-art-of-web.com/system/cron-set-timezone/
# This task, which runs the library import, and queue's everything for re-index, must run @ midnight.
SHELL=/bin/bash
TZ=America/New_York
0 * * * * [ "$(date +\%H\%M)" == "0000" ] && /home/agaric/bin/cronjob.sh 2>&1 > /dev/null
```

And in the referenced cronjob.sh:

```
#!/bin/bash
drush="/var/www/fic/vendor/bin/drush -r /var/www/fic/web/"
$drush migrate:import findit_library_sync_events
$drush search-api:reset-tracker main
```

## MIT Community

TBD
